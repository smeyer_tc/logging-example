I'd like to use slf4j for the majority of my logging needs. 

I would also like to decorate my logs with contextual information about the ambient state surrounding my log messages.


```
@SpringBootApplication
public class LoggingDemoApplication {

	//Used as an example that we could also just use the default slf4j logger and it will
	//be populated with ambient content;
	static Logger defaultLogger = LoggerFactory.getLogger(LoggingDemoApplication.class);

	// Can be instantiated for control over TC log conventions.
	static ILogService tcLogger = LogFactory.getLogger(LoggingDemoApplication.class);

	public static void main(String[] args) {
		// set via http filter, etc.
		// This example adds it prior to spring setup
		// so that correlation id exists in the spring boot log messages.
		tcLogger.setCorrelationId("1235");

		SpringApplication.run(LoggingDemoApplication.class, args);

		//called at the beginning of method, or via AOP with meta data
		tcLogger.setLogStep(GoalStepImpl.BEGIN_FEATURE_STEP);

        //the TC implementation implements Logger so you only need to instantiate ILogService or Logger, not both. 
		tcLogger.info("Implements Logger so all usual log features are available {}", "like params");

		defaultLogger.info("written with slf4j, should have correlation id and feature step");

		//called when http filter is complete, etc.
		tcLogger.clearContext();

		defaultLogger.info("new message with context cleared.");
	}
}
```