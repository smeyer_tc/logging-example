package com.tc.tsp.util.loggingdemo;

import com.tc.tsp.logging.ILogService;
import com.tc.tsp.logging.LogFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LoggingDemoApplication {

	//Used as an example that we could also just use the default slf4j logger and it will
	//be populated with ambient content;
	static Logger defaultLogger = LoggerFactory.getLogger(LoggingDemoApplication.class);

	// Can be instantiated for control over TC log conventions.
	static ILogService logger = LogFactory.getLogger(LoggingDemoApplication.class);

	public static void main(String[] args) {
		// set via http filter, etc.
		// This example adds it prior to spring setup
		// so that correlation id exists in those log calls.
		logger.setCorrelationId("1235");

		SpringApplication.run(LoggingDemoApplication.class, args);

		logger.setLogGoal(GoalImpl.SOME_FEATURE);
		//called at the beginning of method, or via AOP meta data
		logger.setLogStep(GoalStepImpl.STEP_1);
		logger.info("Implements Logger so all usual log features are available {}", "like params");
		logger.setLogStep(GoalStepImpl.STEP_2);
		defaultLogger.info("written with slf4j, should have correlation id and feature step");
		//called when http filter is complete, etc.
		logger.clearContext();

		defaultLogger.info("new message with context cleared.");
	}
}
