package com.tc.tsp.util.loggingdemo;

import com.tc.tsp.logging.ILogStepProvider;

public enum GoalStepImpl  implements ILogStepProvider{
    STEP_1("Step 1"),
    STEP_2("Step 2");

    String step;

    GoalStepImpl(String s){
        step = s;
    }

    public String getLogStep() {
        return step.toString();
    }
}
