package com.tc.tsp.util.loggingdemo;

import com.tc.tsp.logging.ILogGoalProvider;

public enum GoalImpl implements ILogGoalProvider {

    SOME_FEATURE("Some Feature");

    String goal;
    GoalImpl(String s){
        goal = s;
    }

    public String getLogGoal() {
        return goal;
    }
}
