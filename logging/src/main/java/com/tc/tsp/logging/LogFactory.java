package com.tc.tsp.logging;

import org.slf4j.LoggerFactory;

public class LogFactory {
    public static ILogService getLogger(String name){
        return new LogService(name);
    }

    public static ILogService getLogger(Class<?> clazz){
        return getLogger(clazz.getName());
    }
}

