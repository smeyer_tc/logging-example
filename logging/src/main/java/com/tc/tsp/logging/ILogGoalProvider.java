package com.tc.tsp.logging;

public interface ILogGoalProvider {
    String getLogGoal();
}
