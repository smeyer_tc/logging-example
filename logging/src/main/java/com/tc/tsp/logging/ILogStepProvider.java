package com.tc.tsp.logging;

public interface ILogStepProvider {
    String getLogStep();
}
