package com.tc.tsp.logging;

import org.slf4j.Logger;

public interface ILogService extends Logger {
    void setCorrelationId(String correlationId) throws UnexpectedLogException;
    <T extends ILogStepProvider> void setLogStep(T logStepProvider);
    <T extends ILogGoalProvider> void setLogGoal(T logGoalProvider);
    void clearContext();
}
