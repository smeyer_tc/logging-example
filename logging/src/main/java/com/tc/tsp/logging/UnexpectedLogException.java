package com.tc.tsp.logging;

public class UnexpectedLogException extends RuntimeException {
    public UnexpectedLogException(String message){
        super(message);
    }
}
